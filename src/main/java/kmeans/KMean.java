package kmeans;/*
 * KMeans.java ; Cluster.java ; Point.java
 *
 * Solution implemented by DataOnFocus
 * www.dataonfocus.com
 * 2015
 *
*/

import java.util.ArrayList;
import java.util.List;


public class KMean {

    //Number of Clusters. This metric should be related to the number of points
    private int NUM_CLUSTERS = 10;
    //Number of Points
    private int NUM_POINTS = 60000;
    //Min and Max X and Y
    private static final int MIN_COORDINATE = 0;
    private static final int MAX_COORDINATE = 10;

    private List<MeanPoint> points;
    private List<KCluster> clusters;

    public KMean() {
        this.points = new ArrayList();
        this.clusters = new ArrayList();
    }

    public static void main(String[] args) {

        KMean kmeans = new KMean();
        kmeans.init();
        kmeans.calculate();
    }

    //Initializes the process
    public void init() {
        //Create Points
        points = MeanPoint.createRandomPoints(MIN_COORDINATE,MAX_COORDINATE,NUM_POINTS);

        //Create Clusters
        //Set Random Centroids
        for (int i = 0; i < NUM_CLUSTERS; i++) {
            KCluster cluster = new KCluster(i);
            MeanPoint centroid = MeanPoint.createRandomPoint(MIN_COORDINATE,MAX_COORDINATE);
            cluster.setCentroid(centroid);
            clusters.add(cluster);
        }

        //Print Initial state
         plotClusters();
    }

    private void plotClusters() {
        for (int i = 0; i < NUM_CLUSTERS; i++) {
            KCluster c = (KCluster) clusters.get(i);
            c.plotCluster();
        }
    }

    //The process to calculate the K Means, with iterating method.
    public void calculate() {
        boolean finish = false;
        int iteration = 0;

        // Add in new data, one at a time, recalculating centroids with each new one.
        while(!finish) {
            //Clear cluster state
            clearClusters();

            List lastCentroids = getCentroids();

            //Assign points to the closer cluster
            assignCluster();

            //Calculate new centroids.
            calculateCentroids();

            iteration++;

            List currentCentroids = getCentroids();

            //Calculates total distance between new and old Centroids
            double distance = 0;
            for(int i = 0; i < lastCentroids.size(); i++) {
                distance += MeanPoint.distance((MeanPoint) lastCentroids.get(i),(MeanPoint) currentCentroids.get(i));
            }
           // System.out.println("#################");
            System.out.println("Iteration: " + iteration);
            //System.out.println("Centroid distances: " + distance);
           // plotClusters();

            if(distance == 0) {
                finish = true;
                System.out.println("DONE CLUSTERING!");

            }
        }
    }

    private void clearClusters() {
        for(KCluster cluster : clusters) {
            cluster.clear();
        }
    }

    private List getCentroids() {
        List centroids = new ArrayList(NUM_CLUSTERS);
        for(KCluster cluster : clusters) {
            MeanPoint aux = cluster.getCentroid();
            MeanPoint point = new MeanPoint(aux.x,aux.y, aux.ori, aux.scale, aux.ivec);
            centroids.add(point);
        }
        return centroids;
    }

    private void assignCluster() {
        double max = Double.MAX_VALUE;
        double min = max;
        int cluster = 0;
        double distance = 0.0;

        for(MeanPoint point : points) {
            min = max;
            for(int i = 0; i < NUM_CLUSTERS; i++) {
                KCluster c = clusters.get(i);
                distance = MeanPoint.distance(point, c.getCentroid());
                if(distance < min){
                    min = distance;
                    cluster = i;
                }
            }
            point.setCluster(cluster);
            clusters.get(cluster).addPoint(point);
        }
    }

    private void calculateCentroids() {
        for(KCluster cluster : clusters) {
            double sumX = 0;
            double sumY = 0;
            double sumOri = 0;
            double sumScale = 0;
            List<MeanPoint> list = cluster.getPoints();
            int n_points = list.size();

            for(MeanPoint point : list) {
                sumX += point.x;
                sumY += point.y;
                sumOri += point.ori;
                sumOri += point.scale;
            }

            MeanPoint centroid = cluster.getCentroid();
            if(n_points > 0) {
                double newX = sumX / n_points;
                double newY = sumY / n_points;
                double newOri = sumOri / n_points;
                double newScale = sumScale / n_points;
                centroid.x = (float) newX;
                centroid.y = (float) newY;
                centroid.ori = (float) newOri;
                centroid.scale = (float) newScale;
            }
        }
    }
}