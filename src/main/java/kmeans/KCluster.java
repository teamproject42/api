package kmeans;

import java.util.ArrayList;
import java.util.List;

public class KCluster {

    public List<MeanPoint> points;
    public MeanPoint centroid;
    public int id;

    //Creates a new Cluster
    public KCluster(int id) {
        this.id = id;
        this.points = new ArrayList();
        this.centroid = null;
    }

    public List getPoints() {
        return points;
    }

    public void addPoint(MeanPoint point) {
        points.add(point);
    }

    public void setPoints(List points) {
        this.points = points;
    }

    public MeanPoint getCentroid() {
        return centroid;
    }

    public void setCentroid(MeanPoint centroid) {
        this.centroid = centroid;
    }

    public int getId() {
        return id;
    }

    public void clear() {
        points.clear();
    }

    public void plotCluster() {
        System.out.println("[Cluster: " + id + "]");
        System.out.println("[Centroid: " + centroid + "]");
        System.out.println("[Points: \n");
        for (MeanPoint p : points) {
            System.out.println(p);
        }
        System.out.println("]");
    }
}