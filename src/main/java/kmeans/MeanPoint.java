package kmeans;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by johankladder on 1-2-17.
 */
public class MeanPoint {

    public float x = 0;
    public float y = 0;
    public float ori = 0;
    public float scale = 0;
    public byte[] ivec = new byte[128];

    public int cluster_number = 0;

    public MeanPoint(float x, float y, float ori, float scale, byte[] ivec) {
        this.x = x;
        this.y = y;
        this.ori = ori;
        this.scale = scale;
        this.ivec = ivec;
    }

    //Calculates the distance between two points.
    public static double distance(MeanPoint p, MeanPoint centroid) {
        return Math.sqrt(Math.pow((centroid.y - p.y), 2) + Math.pow((centroid.x - p.x), 2) +
                Math.pow((centroid.ori - p.ori), 2) + Math.pow((centroid.scale - p.scale), 2));
    }

    //Creates random point
    public static MeanPoint createRandomPoint(int min, int max) {
        Random r = new Random();
        float x = (float) (min + (max - min) * r.nextDouble());
        float y = (float) (min + (max - min) * r.nextDouble());
        float scale = (float) (min + (max - min) * r.nextDouble());
        float ori = (float) (min + (max - min) * r.nextDouble());
        return new MeanPoint(x,y, scale, ori, new byte[128]);
    }

    public void setCluster(int n) {
        this.cluster_number = n;
    }

    public static List createRandomPoints(int min, int max, int number) {
        List points = new ArrayList(number);
        for(int i = 0; i< number; i++) {
            points.add(createRandomPoint(min,max));
        }
        return points;
    }

    public String toString() {
        return "("+x+","+y+","+scale+","+ori+")";
    }


}
