package main;

import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;
import saver.KeypointSaver;
import util.Image;
import util.KeyPointsUtil;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by johankladder on 27-1-17.
 */
public class Feeder {

    public static int NUMBER_OF_THREADS = 10;

    // Thread pool
    private static ExecutorService executor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public static void main(String[] args) {
        Feeder.loadFeed();
    }

    public static void loadFeed() {
        URL resource = Main.class.getResource("/feed/");
        File dir = new File(resource.getFile());
        File[] dirList = dir.listFiles();
        prepareMultiThreading(dirList);
    }

    private static void prepareMultiThreading(File[] dirList) {
        List<Callable<Object>> todo = new ArrayList<>();
        for (File child : dirList) {
            todo.add(Executors.callable(() -> createAndSave(child)));
        }

        try {
            List<Future<Object>> futures = executor.invokeAll(todo);
            System.out.println("All feeding done.");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void createAndSave(File child) {
        KeypointSaver saver = new KeypointSaver();
        try {
            FImage image = ImageUtilities.readF(child);
            Image image1 = new Image(KeyPointsUtil.extractKeyPointFromImage(image), child.getName(), "testDescription");
            saver.saveImage(image1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
