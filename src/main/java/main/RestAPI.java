package main;

import com.mongodb.client.MongoDatabase;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.util.JSON;
import loader.KeyPointsLoader;
import matcher.ImageMatcher;
import org.bson.conversions.Bson;
import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.processing.resize.ResizeProcessor;
import saver.KeypointSaver;
import spark.Request;
import spark.Response;
import util.Image;
import util.KeyPointsUtil;

import javax.servlet.MultipartConfigElement;
import javax.servlet.http.Part;
import java.io.IOException;

/**
 * Created by johankladder on 27-1-17.
 */
public class RestAPI {

    public static Object match(Request request, Response response) {
        MultipartConfigElement multipartConfigElement = new MultipartConfigElement("/tmp");
        request.raw().setAttribute("org.eclipse.jetty.multipartConfig", multipartConfigElement);
        Part file;
        try {
            System.out.println("TEST");
            file = request.raw().getPart("file");

            System.out.println("Create image...");
            FImage image = ImageUtilities.readF(file.getInputStream());
            image = ResizeProcessor.resizeMax(image, 500);

            System.out.println("Start matching...");
            ImageMatcher imageMatcher = new ImageMatcher();
            Image result = imageMatcher.match(image);
            System.out.println("MATCH FOUND: " + result.getName());

            result.document.remove("keypoint");
            response.type("application/json");
            response.status(200);
            return JSON.serialize(result.document);

        } catch (Exception e) {
            e.printStackTrace();
        }
        response.status(500);
        return response;
    }

    public static Object saveToDatabase(Request request, Response response) {
        System.out.println("SAVED!");
        MultipartConfigElement multipartConfigElement = new MultipartConfigElement("/tmp");
        request.raw().setAttribute("org.eclipse.jetty.multipartConfig", multipartConfigElement);
        Part file;
        try {
            file = request.raw().getPart("file"); //file is name of the upload form
            FImage image = ImageUtilities.readF(file.getInputStream());
            image = ResizeProcessor.resizeMax(image, 500);


            KeypointSaver keypointSaver = new KeypointSaver();
            Image image1 = new Image(KeyPointsUtil.extractKeyPointFromImage(image), request.queryParams("name"), request.queryParams("description"));
            image1.image = file.getInputStream();
            keypointSaver.saveImage(image1);

            response.status(200);
            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }
        response.status(500);
        return response;
    }

    public static Object getImageData(Request request, Response response) {
        System.out.println("TEST");
        KeyPointsLoader loader = new KeyPointsLoader();
        GridFSDBFile id = loader.retrieveImageData(request.queryParams("id"));



        response.type(id.getContentType());

        try {
            id.writeTo(response.raw().getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }
}
