package main;

import static spark.Spark.get;
import static spark.Spark.port;
import static spark.Spark.post;

/**
 * Created by max on 25-1-17.
 */
    public class Main {


    public static void main(String[] args) {
        port(1337);
        post("/save", RestAPI::saveToDatabase);
        post("/match", RestAPI::match);
        get("/hello", (req, res) -> "Hello World");
        get("/imagedata", RestAPI::getImageData);
    }
}
