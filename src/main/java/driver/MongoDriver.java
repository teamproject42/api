package driver;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

/**
 * Class that is capable of controlling the MongoDB communication. It will create a database instance only
 * once. Please use this class when performing database queries.
 */
public class MongoDriver {

    // The location of MONGO:
    private static final String MONGO_LOCATION = "localhost";

    // Port number that is used by MONGO:
    private static final int MONGO_PORT = 27017;

    // The database-name:
    protected static final String DB_NAME = "project42";

    // The database collection that needs to be used:
    public static String collection = "entities";

    protected static MongoClient MONGO_CLIENT = new MongoClient(MONGO_LOCATION, MONGO_PORT);

    protected static MongoDatabase db = MONGO_CLIENT.getDatabase(DB_NAME);

    protected static void saveObjectInCollection(Document object) {
        db.getCollection(collection).insertOne(object);
        System.out.println("Save done in collection: " + collection);
    }

    /**
     * Loads all the entities from the database.
     * @return Entities iterable list
     */
    public static FindIterable<Document> loadCollection() {
        return db.getCollection(collection).find();
    }

    /**
     * Method for deleting all the entities in the database. Is only used in tests.
     * // TODO: Remove this method and place it into test env.
     */
    public static void dropCollection() {
        db.getCollection(collection).drop();
    }


}
