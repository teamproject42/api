package matcher;


import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import loader.KeyPointsLoader;
import org.openimaj.feature.local.list.LocalFeatureList;
import org.openimaj.feature.local.matcher.FastBasicKeypointMatcher;
import org.openimaj.feature.local.matcher.LocalFeatureMatcher;
import org.openimaj.feature.local.matcher.consistent.ConsistentLocalFeatureMatcher2d;
import org.openimaj.image.FImage;
import org.openimaj.image.feature.local.keypoints.Keypoint;
import org.openimaj.image.processing.resize.ResizeProcessor;
import org.openimaj.math.geometry.transforms.HomographyRefinement;
import org.openimaj.math.geometry.transforms.estimation.RobustHomographyEstimator;
import org.openimaj.math.model.fit.RANSAC;
import util.Image;
import util.KeyPointsUtil;

import java.io.InputStream;
import java.util.*;
import java.util.concurrent.*;

public class ImageMatcher {

    // Max number of threads that are used by the thread-pool:
    private static int NUMBER_OF_THREADS = 20;

    // Thread pool:
    private ExecutorService executor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    // Loader for loading the keypoints that are saved in the database
    private KeyPointsLoader loader = new KeyPointsLoader();

    // Map containing the matches with their hit ratio's
    private HashMap<Image, Float> hitRatios = new HashMap<>();


    /**
     * Matches the given images to images to the database. This method calls the ratio creation method and will after it
     * sort the ratio's. This method will return the image with the highest match-ratio.
     *
     * @param imageToCompare The image like to be compared
     * @return Image object that had the highest match according to the imageToCompare
     */
    public Image match(FImage imageToCompare) {
        HashMap<Image, Float> RatioMap = createRatioMap(KeyPointsUtil.extractKeyPointFromImage(imageToCompare));
        Map.Entry<Image, Float> maxEntry = null;

        for (Map.Entry<Image, Float> entry : RatioMap.entrySet()) {
            if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0) {
                maxEntry = entry;
            }
        }

        sortByValue(RatioMap);
        if(maxEntry != null) {
            return maxEntry.getKey();
        }
        return null;
    }



    /**
     * Returns an map containing all the images from the database including with their hit ratio according the the feed
     * image that was given. The matching in this method will occur multithreaded with the number of threads that where
     * given in the NUMBER_OF_THREADS field. This field can therefore be incremented or lowered. Default = 3 threads.
     *
     * @param keyPointsReceivedImage The feed image (Image you would like to compare)
     * @return Map containing the hit ratios from found images and information.
     */
    public HashMap<Image, Float> createRatioMap(LocalFeatureList<Keypoint> keyPointsReceivedImage) {
        ArrayList<Image> keyPointsDatabase = getKeyPointsFromDatabase();

        List<Callable<Object>> todo = new ArrayList<>();

        for (Image image : keyPointsDatabase) {
            todo.add(Executors.callable(() -> getMatches(keyPointsReceivedImage, image)));
        }

        try {
            executor.invokeAll(todo);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return hitRatios;
    }


    /**
     * Finds matches between the key-points of the recieved image and an key-points entry from the database. The found
     * hit ratio is stored in the ratio-map
     *
     * @param keyPointsReceivedImage List containing the key-points of the feed-image
     * @param keyPointFromDatabase   ImagePoint object containing key-points and image information that were contained from
     *                               the database.
     */
    private void getMatches(LocalFeatureList<Keypoint> keyPointsReceivedImage, Image keyPointFromDatabase) {
        LocalFeatureMatcher<Keypoint> matcher = createConsistentRANSACHomographyMatcher(); // TODO: MAKE NOT STatic
        System.out.println("GET MATCHES from: " + keyPointFromDatabase.getName() + " | IN: " + Thread.currentThread().getName());
        LocalFeatureList<Keypoint> keypointsDatabase = keyPointFromDatabase.getKeypoints();
        matcher.setModelFeatures(keyPointsReceivedImage);
        matcher.findMatches(keypointsDatabase);
        storeImageWithHitRatio(keyPointFromDatabase, calculateHitScore(keypointsDatabase.size(), matcher.getMatches().size()));
    }



    /*
    Method copied from OPENIMAJ:
     */
    private static LocalFeatureMatcher<Keypoint> createConsistentRANSACHomographyMatcher() {
        final ConsistentLocalFeatureMatcher2d<Keypoint> matcher = new ConsistentLocalFeatureMatcher2d<Keypoint>(
                createFastBasicMatcher());
        matcher.setFittingModel(new RobustHomographyEstimator(10.0, 1000, new RANSAC.BestFitStoppingCondition(),
                HomographyRefinement.NONE));

        return matcher;
    }

    /*
    Simple methods:
     */
    private ArrayList<Image> getKeyPointsFromDatabase() {
        return loader.loadKeyPoints();
    }

    private float calculateHitScore(float totalKeyPoints, float hits) {

        return (hits / totalKeyPoints) * 100;
    }

    private void storeImageWithHitRatio(Image image, float score) {
        hitRatios.put(image, score);
    }

    private static LocalFeatureMatcher<Keypoint> createFastBasicMatcher() {
        return new FastBasicKeypointMatcher<>(8);
    }

    private static <K, V extends Comparable<? super V>> Map<K, V>
    sortByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list =
                new LinkedList<Map.Entry<K, V>>(map.entrySet());
        Collections.sort(list, (o1, o2) -> (o1.getValue()).compareTo(o2.getValue()));

        Map<K, V> result = new LinkedHashMap<K, V>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }
}
