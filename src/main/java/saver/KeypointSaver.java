package saver;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSInputFile;
import driver.MongoDriver;
import org.bson.Document;
import org.openimaj.feature.local.list.LocalFeatureList;
import org.openimaj.image.feature.local.keypoints.Keypoint;
import util.Image;
import util.KeyPointsUtil;

public class KeypointSaver extends MongoDriver {


    /**
     * Saves an ImageObject to the database. This image needs to have and list containing keypoints and a name. The
     * object is parsed into an document.
     * @param image Image liked to be inserted into the database.
     */
    public void saveImage(Image image) {
        Document databaseEntity = parseKeyPointsToDocument(image.getKeypoints());

        GridFS gfsPhoto = new GridFS(MONGO_CLIENT.getDB(DB_NAME), "photo");

        if(image.image != null) {
            GridFSInputFile gfsFile = gfsPhoto.createFile(image.image);
            image.idPic = gfsFile.getId().toString();
            gfsFile.setFilename(image.getName());
            gfsFile.save();
        }

        addAdditionalInfomation(databaseEntity, image.getName(), image.getDescription(), image.idPic);
        saveObjectInCollection(databaseEntity);
    }


    /**
     * Parses an list with key points to an entity that can be saved into a database: a document.
     * @param keyPoints List containing all the key points from the image.
     * @return Document containing key point information
     */
    private Document parseKeyPointsToDocument(LocalFeatureList<Keypoint> keyPoints) {


        BasicDBList list = new BasicDBList();

        for (int i = 0; i < keyPoints.size(); i++) {
            Keypoint keypoint = keyPoints.get(i);
            BasicDBObject objectKeyPoints = new BasicDBObject();
            objectKeyPoints.put(KeyPointsUtil.X_PREFIX, keypoint.x);
            objectKeyPoints.put(KeyPointsUtil.Y_PREFIX, keypoint.y);
            objectKeyPoints.put(KeyPointsUtil.ORI_PREFIX, keypoint.ori);
            objectKeyPoints.put(KeyPointsUtil.IVEC_PREFIX, keypoint.ivec);
            objectKeyPoints.put(KeyPointsUtil.SCALE_PREFIX, keypoint.scale);
            list.add(objectKeyPoints);
        }

        return new Document(KeyPointsUtil.KEYPOINT_PREFIX, list);

    }

    private void addAdditionalInfomation(Document document, String name, String description, Object picRef) {
        document.put(KeyPointsUtil.NAME_PREFIX, name);
        document.put(KeyPointsUtil.DESCRIPTION_PREFIX, description);
        document.put("imageref", picRef);
    }

}
