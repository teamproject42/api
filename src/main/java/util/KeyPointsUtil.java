package util;

import org.openimaj.feature.local.list.LocalFeatureList;
import org.openimaj.feature.local.list.MemoryLocalFeatureList;
import org.openimaj.image.FImage;
import org.openimaj.image.feature.local.engine.asift.ASIFTEngine;
import org.openimaj.image.feature.local.keypoints.Keypoint;

import java.util.ArrayList;

public class KeyPointsUtil {

    public static final String X_PREFIX = "x";
    public static final String Y_PREFIX = "y";
    public static final String ORI_PREFIX = "ori";
    public static final String SCALE_PREFIX = "scale";
    public static final String IVEC_PREFIX = "ivec";
    public static final String NAME_PREFIX = "name";
    public static final String DESCRIPTION_PREFIX = "description";
    public static final String KEYPOINT_PREFIX = "keypoint";

    /**
     * Creates an list containing all the keypoints that the ASIFT-engine could find.
     * @param image Feed image
     * @return List containing key points
     */
    public static LocalFeatureList<Keypoint> extractKeyPointFromImage(FImage image) {
        ASIFTEngine engine = new ASIFTEngine(false, 7);
        System.out.println("Extracting keypoints from image...");
        return engine.findKeypoints(image);
    }

    public static LocalFeatureList<Keypoint> createLocalFeatureList(ArrayList<Keypoint> keypoints) {
        MemoryLocalFeatureList<Keypoint> list = new MemoryLocalFeatureList<>(keypoints);
        return list;
    }

}
