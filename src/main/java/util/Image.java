package util;

import org.bson.Document;
import org.openimaj.feature.local.list.LocalFeatureList;
import org.openimaj.image.FImage;
import org.openimaj.image.feature.local.keypoints.Keypoint;

import java.io.InputStream;

/**
 * Object that stores all the information that is saved in the database
 */

public class Image {
    private LocalFeatureList<Keypoint> keypoints;

    private String name;

    private String description;
    public Document document;

    public Object idPic;

    public InputStream image;

    public Image(LocalFeatureList<Keypoint> keypoints, String name, String description) {
        this.keypoints = keypoints;
        this.name = name;
        this.description = description;
    }

    public LocalFeatureList<Keypoint> getKeypoints() {
        return keypoints;
    }

    public String getName() {
        return name;
    }

    public String getDescription() { return description; }

}

