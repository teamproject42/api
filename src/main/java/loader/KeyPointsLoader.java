package loader;

import com.mongodb.BasicDBList;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCursor;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSFile;
import driver.MongoDriver;
import org.bson.Document;
import org.bson.types.Binary;
import org.bson.types.ObjectId;
import org.openimaj.image.feature.local.keypoints.Keypoint;
import util.Image;
import util.KeyPointsUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;


public class KeyPointsLoader extends MongoDriver {

    ExecutorService executor = Executors.newFixedThreadPool(4);
    /**
     * Loads all entities that were saved in the database and stores them in an Image object.
     * @return List containing the parsed entities.
     */
    public ArrayList<Image> loadKeyPoints() {
        ArrayList<Image> list = new ArrayList<>();
        iterate(MongoDriver.loadCollection(), list);
        return list;
    }

    /**
     * Iterate's through all the entities that are contained from the database via the loadCollection method. After extraction
     * it will add parsed entities through a list.
     * @param cursor Cursor for obtaining entities from the database
     * @param list list where entities can be added to.
     * @see MongoDriver#loadCollection()
     */
    private void iterate(FindIterable<Document> iterable, ArrayList<Image> list) {
        List<Callable<Object>> todo = new ArrayList<>();

        MongoCursor cursor = iterable.iterator();
        while (cursor.hasNext()) {
            Document rootObject = (Document) cursor.next();
            ArrayList<Document> keyPoints = (ArrayList<Document>) rootObject.get(KeyPointsUtil.KEYPOINT_PREFIX);
            todo.add(Executors.callable(() -> insertIntoList(keyPoints, rootObject, list)));


        }
        try {
            List<Future<Object>> futures = executor.invokeAll(todo);
            System.out.println("DONE JOBS");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public GridFSDBFile retrieveImageData(String id) {
        GridFS gfsPhoto = new GridFS(MONGO_CLIENT.getDB(DB_NAME), "photo");
        GridFSDBFile gridFSDBFile = gfsPhoto.find(new ObjectId(id));
        return gridFSDBFile;
    }

    /**
     * Inserts entities into a given list. It will call the parseEntity method from this class to make sure the key points
     * are parsed correctly.
     * @param keypointsDB Unparsed document containing an array with key point information
     * @param rootObject Unparsed document of the complete entity, so other information than key points can be obtained.
     * @param pointsList List where entities can be added to after parsing
     */
    private void insertIntoList(ArrayList<Document> keypointsDB, Document rootObject, ArrayList<Image> pointsList) {
        ArrayList<Keypoint> keypoints = new ArrayList<>();
        for (int i = 0; i < keypointsDB.size(); i++) {
            keypoints.add(createKeyPoint(keypointsDB.get(i)));
        }
        Image image = new Image(KeyPointsUtil.createLocalFeatureList(keypoints), rootObject.get(KeyPointsUtil.NAME_PREFIX).toString(),
                rootObject.get(KeyPointsUtil.DESCRIPTION_PREFIX).toString());

        image.idPic = rootObject.getString("imageref");

        image.document = rootObject;
        pointsList.add(image);
    }


    /**
     * Parses an given document to an KeyPoint object.
     * @param object Unparsed document containing KeyPoint information
     * @return Created KeyPoint from parsed document.
     * @see Keypoint
     */
    private Keypoint createKeyPoint(Document object) {
        double x = (double) object.get(KeyPointsUtil.X_PREFIX);
        double y = (double) object.get(KeyPointsUtil.Y_PREFIX);
        double ori = (double) object.get(KeyPointsUtil.ORI_PREFIX);
        double scale = (double) object.get(KeyPointsUtil.SCALE_PREFIX);
        Binary ivecBin = (Binary) object.get(KeyPointsUtil.IVEC_PREFIX);
        byte[] ivec = ivecBin.getData();
        return new Keypoint((float) x, (float) y, (float) ori, (float) scale, ivec);
    }


}
