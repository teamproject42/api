package loader;

import cases.DatabaseCase;
import org.junit.Test;
import org.openimaj.feature.local.list.LocalFeatureList;
import org.openimaj.image.feature.local.keypoints.Keypoint;
import saver.KeypointSaver;
import util.Image;

import java.util.ArrayList;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * Created by johankladder on 25-1-17.
 */
public class KeyPointsLoaderTest extends DatabaseCase {

    private final String TEST_CASE = "testcase";

    private KeyPointsLoader loader;
    private KeypointSaver saver;

    @Override
    public void before() {
        loader = new KeyPointsLoader();
        saver = new KeypointSaver();
        saver.saveImage(new Image(KEYPOINTS_MAIN_FEED, TEST_CASE, "description"));
    }

    @Test
    public void extractEntities() throws Exception {
        ArrayList<Image> keypointsList = loader.loadKeyPoints();
        assertEquals(1, keypointsList.size());

        for (int i = 0; i < keypointsList.size(); i++) {
            Image image = keypointsList.get(i);
            LocalFeatureList<Keypoint> keypoints = image.getKeypoints();

            assertEquals(TEST_CASE, image.getName());

            for (int keyPoint = 0; keyPoint < keypoints.size(); keyPoint++) {
                Keypoint expected = KEYPOINTS_MAIN_FEED.get(keyPoint);
                Keypoint actual = keypoints.get(keyPoint);

                assertEquals(expected.x, actual.x, 0.0001);

                assertEquals(expected.y, actual.y, 0.0001);

                assertArrayEquals(expected.ivec, actual.ivec);

                assertEquals(expected.scale, actual.scale, 0.0001);

                assertEquals(expected.ori, actual.ori, 0.0001);
            }
        }

    }

}