package cases;

import driver.MongoDriver;
import org.junit.Before;
import org.openimaj.feature.local.list.LocalFeatureList;
import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.feature.local.keypoints.Keypoint;
import util.KeyPointsUtil;

import java.io.IOException;

/**
 * Created by johankladder on 25-1-17.
 */
public abstract class DatabaseCase {

    private final String TEST_COLLECTION = "testentities";
    private final String MAIN_FEED = "/matcher/input_0.png";
    protected static LocalFeatureList<Keypoint> KEYPOINTS_MAIN_FEED;
    protected static FImage IMAGE_MAIN_FEED;


    @Before
    public void setup() throws IOException {
        prepareDatabase();
        createMainFeed();
        before();
    }

    private void prepareDatabase() {
        MongoDriver.collection = TEST_COLLECTION;
        MongoDriver.dropCollection();
    }

    private void createMainFeed() throws IOException {
        if (KEYPOINTS_MAIN_FEED == null && IMAGE_MAIN_FEED == null) {
            IMAGE_MAIN_FEED = ImageUtilities.readF(this.getClass().getResource(MAIN_FEED));
            KEYPOINTS_MAIN_FEED = KeyPointsUtil.extractKeyPointFromImage(IMAGE_MAIN_FEED);
        }
    }


    public abstract void before();

}
