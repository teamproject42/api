package kmeans;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Point {

    private double x = 0;
    private double y = 0;
    private double scale = 0;
    private double ori = 0;

    private int cluster_number = 0;

    public Point(double x, double y, double scale, double ori)
    {
        this.setX(x);
        this.setY(y);
        this.setScale(scale);
        this.setOri(ori);
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getX()  {
        return this.x;
    }

    public double getOri() {
        return this.ori;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getY() {
        return this.y;
    }

    public void setScale(double scale) {
        this.scale = scale;
    }

    public double getScale() {
        return this.scale;
    }

    public void setCluster(int n) {
        this.cluster_number = n;
    }

    public int getCluster() {
        return this.cluster_number;
    }

    //Calculates the distance between two points.
    public static double distance(Point p, Point centroid) {
        return Math.sqrt(Math.pow((centroid.getY() - p.getY()), 2) + Math.pow((centroid.getX() - p.getX()), 2));
    }

    //Creates random point
    public static Point createRandomPoint(int min, int max) {
        Random r = new Random();
        double x = min + (max - min) * r.nextDouble();
        double y = min + (max - min) * r.nextDouble();
        double scale = min + (max - min) * r.nextDouble();
        double ori = min + (max - min) * r.nextDouble();
        return new Point(x,y, scale, ori);
    }

    public static List createRandomPoints(int min, int max, int number) {
        List points = new ArrayList(number);
        for(int i = 0; i< number; i++) {
            points.add(createRandomPoint(min,max));
        }
        return points;
    }

    public String toString() {
        return "("+x+","+y+")";
    }

    public void setOri(double ori) {
        this.ori = ori;
    }
}