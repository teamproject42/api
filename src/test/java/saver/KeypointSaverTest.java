package saver;

import cases.DatabaseCase;
import com.mongodb.DBCursor;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCursor;
import driver.MongoDriver;
import org.bson.Document;
import org.junit.Test;
import util.Image;

import static org.junit.Assert.assertEquals;

/**
 * Created by johankladder on 25-1-17.
 */
public class KeypointSaverTest extends DatabaseCase {

    private KeypointSaver saver;

    @Override
    public void before() {
        saver = new KeypointSaver();
    }

    @Test
    public void save() throws Exception {
        saver.saveImage(new Image(KEYPOINTS_MAIN_FEED, "input0.png", "des"));
        FindIterable<Document> load = MongoDriver.loadCollection();
        MongoCursor cursor = load.iterator();

        int count = 0;
        while(cursor.hasNext()) {
            count++;
            cursor.next();
        }
        assertEquals(1, count);

    }

}