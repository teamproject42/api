package matcher;

import cases.DatabaseCase;
import org.junit.Test;
import org.openimaj.image.ImageUtilities;
import saver.KeypointSaver;
import util.Image;
import util.KeyPointsUtil;

import java.io.IOException;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by johankladder on 25-1-17.
 */
public class ImageMatcherTest extends DatabaseCase {

    private final String HIT_IMAGE = "hitimage";

    private KeypointSaver saver = new KeypointSaver();
    private ImageMatcher matcher = new ImageMatcher();

    @Override
    public void before() {
        try {
            saver.saveImage(new Image(KeyPointsUtil.extractKeyPointFromImage(ImageUtilities.readF(this.getClass().getResourceAsStream("/matcher/input_1.png"))), HIT_IMAGE, "desc"));
            saver.saveImage(new Image(KeyPointsUtil.extractKeyPointFromImage(ImageUtilities.readF(this.getClass().getResourceAsStream("/matcher/input_2.png"))), "input2", "desc"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void createRatioMap() throws Exception {
        HashMap<Image, Float> matchRatio = matcher.createRatioMap(KEYPOINTS_MAIN_FEED);
        assertTrue(matchRatio.size() > 1);
        matchRatio.forEach((image, ratio) -> {

            System.out.println("NAME: " + image.getName() +" | HIT-RATIO: " + ratio);
            if (image.getName().equals(HIT_IMAGE)) {
                assertTrue(ratio > 30);
            } else {
                assertTrue(ratio < 30);
            }
        });


    }

    @Test
    public void matchTest() {
        Image match = matcher.match(IMAGE_MAIN_FEED);
        assertEquals(HIT_IMAGE, match.getName());
    }

}